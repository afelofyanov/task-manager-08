package ru.tsc.felofyanov.tm.api;

import ru.tsc.felofyanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
